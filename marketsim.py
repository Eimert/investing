#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler as ep

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys, csv, argparse, os, cStringIO, quandl

# Author      : Eimert Vink
# Created     : 18-08-2017
# Generation  : 1st generation
# Description : computational investing coursera market simulator to analyze portfolio performance.
# flow: trade -> check price -> update ownership -> update cash.
# output: stdout or csv with date and total value of portfolio using adjusted close prices.
# Usage       : see tail of this file for examples.
# Verification: The 1st and 2nd generation of this script deliver the same numbers. 2nd Gen code is refactored. Tested 2017-10-22 using orders.csv.

# Declare global variables
df = [] # will hold orders from inputfile
transactions = pd.DataFrame() # is based on df list
stockdata = pd.DataFrame()
ownership = {}
cashreg = {}
quandl.ApiConfig.api_key = '2GPdwxwGtTARrXz21oQR' # bugmenot

def is_valid_file(parser, arg):
    if not os.path.isfile(arg):
      parser.error("The file %s does not exist!" % arg)
    else:
      return arg
        # return open(arg, 'r')  # return an open file handle

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def arguments(args=None):
  parser = argparse.ArgumentParser(description='Market simulator that computes how a portfolio would perform. Input: 1. Amount of cash in $, 2. orders.csv (transactions file), 3. output file for writing (values.csv)')
  parser.add_argument('--cash', type=int, action='store', dest='cash', required=True, help='an integer for the portfolio cash.')
  parser.add_argument('--transactions', action='store', dest='orderfile', metavar='FILE', required=True, help='Input transaction file, ex.: orders.csv', type=lambda x: is_valid_file(parser, x))
  parser.add_argument('--output', action='store', dest='outputfile', metavar='FILE', required=False, default='./report.csv', help='File for outputting the portfolio performance, ex.: values.csv')
  parser.add_argument('--datasource', action='store', dest='datasource', required=False, default='quandl', help='Specify the datasource: QSTK or quandl.')
  args = parser.parse_args()
  # print args.accumulate(args.integers)
  return args

def __unicode__(self):
  return unicode(self.some_field) or u''

def read_orders(b_file):
  # reader = csv.reader(open(b_file), delimiter=',', quotechar='"')
  # data = open(b_file, 'rb').readlines()
  # reader = csv.DictReader(cStringIO.StringIO(data), delimiter=',')
  # for row in reader:
  #   # print(' '.join(row))
  #   print row
  # for line in data:
    # print(line, '\n')
    # './orders.csv'
  global df
  if 'event' not in b_file:
    df = pd.read_csv(b_file, header=None, parse_dates={'date': [0,1,2]})
    # data cleaning
    df = df.drop(6, axis=1)               # drop the NaN column
  else:
    df = pd.read_csv(b_file, header=None, parse_dates={'date': [0]})
  # data cleaning, always:
  df = df.sort_values(by='date')        # sort by date, will mess up rownums (so those are not sorted anymore)
  df = df.reset_index(drop=True)        # recreate the index (row nums) and drop the old ordering
  # transactions = pd.DataFrame(np.zeros((len(data), 4)),columns=['date', 'symbol', 'action', 'count'])
  # for y in xrange(len(data)):
  #   transactions.date[y] = dt.datetime(*map(int, data[y].split(',')[0:3]))
  #   transactions.symbol[y] = data[y].split(',')[3]
  #   transactions.action[y] = data[y].split(',')[4]
  #   transactions.count[y] = data[y].split(',')[5]


def setup_data_structures():
  print 'DEBUG:', sys._getframe().f_code.co_name
  # cashreg
  # dates from begin to end to dict
  global transactions, cashreg, ownership
  transactions = pd.DataFrame(df.values,columns=['date', 'symbol', 'action', 'count'])
  transactions = transactions.sort_values(by='date')
  transactions = transactions.set_index('date')
  #
  if args.datasource == 'QSTK':
    print 'QSTK: no data cleaning on input orderfile.'
  elif args.datasource == 'quandl':
    print 'Changing GOOG into GOOGL'
    transactions.replace('GOOG','GOOGL', inplace=True)
  else:
    print 'Unrecognized datasource given'
    return 1
  # d = {'date' : pd.date_range(start=transactions.index.min(), end=transactions.index.max(), freq='D')}
  # d['amount'] = np.zeros((len(d['date'])))
  # cashreg = pd.DataFrame(d,columns=['date', 'cash', 'unrealized_yield', 'realized_yield', 'total_value']) # total_value = cash + unrealized_yield
  # cashreg structure: ['date', 'cash', 'unrealized_yield', 'realized_yield', 'total_value']
  for timestamp in pd.date_range(start=transactions.index.min(), end=transactions.index.max(), freq='D'):
    cashreg[timestamp] = {'cash': 0, 'total_assets': 0, 'realized_yield': 0}
    # ownership[timestamp] = {}
  # pd dataframe are not suitable for frequent CRUD operations
  cashreg[transactions.index.min()].update({'cash': args.cash})
  ownership = {}
  # ownership = pd.DataFrame({'symbol' : '', 'cost_price' : '', 'count' : ''},index=[0],columns=['symbol', 'cost_price', 'count'])

def download_stockdata():
  print 'DEBUG:', sys._getframe().f_code.co_name
  global stockdata
  # using quandl: limited for anonymous users
  # http://help.quandl.com/article/206-how-do-i-download-a-specific-date-range-using-python
  # for each stock: download
  for symbol in transactions['symbol'].unique():
    # stockdata.append(quandl.get("WIKI/" + symbol, start_date=transactions.index.min(), end_date=transactions.index.max()))
    data = quandl.get("WIKI/" + symbol, start_date=transactions.index.min(), end_date=transactions.index.max())['Adj. Close']
    assert(len(data) != 0), "quandl did return an empty dataset"
    stockdata.insert(loc=len(stockdata.keys()), column=symbol, value=data)

def parse_stockdata():
  print 'DEBUG:', sys._getframe().f_code.co_name
  global stockdata
  # use QSTK files
  # Creating an object of the dataaccess class with Yahoo as the source.
  c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
  ls_symbols = c_dataobj.get_symbols_from_list("sp5002012")
  dt_start = transactions.index.min()
  dt_end = transactions.index.max() + dt.timedelta(days=1)
  # We need closing prices so the timestamp should be hours=16.
  dt_timeofday = dt.timedelta(hours=16)
  # Get a list of trading days between the start and the end.
  ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
  # Keys to be read from the data, it is good to read everything in one go.
  # ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
  ls_keys = ['actual_close']
  stockdata = c_dataobj.get_data(ldt_timestamps, list(transactions['symbol'].unique()), ls_keys)[0]
  # to-do: alert when a symbol could not be found
  # timestamps are now in hours == 16.00, whereas transactions expect hours == 00.00. changing it.
  # stockdata.index -= pd.DateOffset(hours=dt_timeofday)
  stockdata.index -= dt_timeofday

def process_single(day, action, symbol, count, transaction_sum):
  # global ownership,cashreg
  # process single
  #
  # cashreg update.
  if action == 'Buy':
    new_cashlevel = (cashreg[day]['cash'] - transaction_sum)
  if action == 'Sell':
    new_cashlevel = (cashreg[day]['cash'] + transaction_sum)
    count = -count
  print 'Old cashlevel:', cashreg[day]['cash'], 'new cashlevel:', new_cashlevel
  cashreg[day].update({'cash': new_cashlevel})
  #
  # ownership update.
  if symbol in ownership:
    new_tr_sum = (ownership[symbol]['cost_price'] + (count * (ownership[symbol]['cost_price'] / ownership[symbol]['count'])))
    new_count = (ownership[symbol]['count'] + count)
    if new_count == 0:
      ownership.pop(symbol)
      print 'Removed ownership: ', symbol
    else:
      ownership[symbol].update({'cost_price': new_tr_sum, 'count': new_count})
      print 'Updated ownership: ', symbol, ' ', ownership[symbol]
  else:
    ownership[symbol] = {'cost_price': transaction_sum, 'count': count}
    print 'Added ownership: ', symbol, ' ', ownership[symbol]

def process_transactions():
  print 'DEBUG:', sys._getframe().f_code.co_name
  # reference:
  # >>> transactions.keys() == Index([u'date', u'symbol', u'action', u'count'], dtype='object')
  # ownership structure: ownership[key] = ['cost_price', 'count']
  # cashreg[timestamp] = {'cash': 0, 'total_assets': 0, 'realized_yield': 0}
  global ownership, cashreg
  for day in sorted(cashreg.keys()):
    if day > transactions.index.min():
      # print "update today's cash with yesterdays value"
      cashreg[day].update({'cash': cashreg[day-1]['cash']})
      # print 'day: ', day,' cashlevel: ', cashreg[day]['cash']
    # buy/sell: update cash, realized_yield, ownership dict.
    if day in transactions.index:
      # not pretty code with repetition, because there can be multiple transactions on one day.
      if isinstance(transactions.ix[day]['action'], str):
        # set variables
        cur_sym = transactions.ix[day]['symbol']
        action = transactions.ix[day]['action']
        order_count = transactions.ix[day]['count']
        print 'day:',day,'Symbol:',cur_sym,'action:',action,' ',order_count#,'total sum:', transaction_sum
        #
        price = stockdata[cur_sym][day]
        transaction_sum = (price * order_count)
        process_single(day=day, action=action, symbol=cur_sym, count=order_count, transaction_sum=transaction_sum)
      else: #type is pandas.core.series.Series
        # in case there are multiple, iterate over the number of transactions that day
        # process multiple
        for num in range(len(transactions.ix[day].values)):
          cur_sym = transactions.ix[day]['symbol'][num]
          action = transactions.ix[day]['action'][num]
          order_count = transactions.ix[day]['count'][num]
          print 'day:',day,'Symbol:',cur_sym,'action:',action,' ',order_count#,'total sum:', transaction_sum
          #
          price = stockdata[cur_sym][day]
          transaction_sum = (price * order_count)
          # process single
          process_single(day=day, action=action, symbol=cur_sym, count=order_count, transaction_sum=transaction_sum)
        # show a value: cashreg[cashreg.keys()[0]]
      # always: update cashreg unrealized_yield for this day, based on ownership.
      # optional for now.
    total_assets = 0.0
    for symbol in ownership.keys():
      hasday = day
      while (hasday not in stockdata[symbol].keys()):
        hasday -= dt.timedelta(days=1)
      count = ownership[symbol]['count']
      last_price = stockdata[symbol][hasday]
      total_assets += (count * last_price)
    # cashreg[day].update({'unrealized_yield': 80})
    # cashreg[transactions.index.min() + dt.timedelta(days=14)]
    # calculate total_assets.
    cashreg[day].update({'total_assets': total_assets})


def output_cashreg(outputfile='./report.csv'):
  print 'DEBUG:', sys._getframe().f_code.co_name
  global cashreg
  with open(outputfile, 'wb') as f:
    w = csv.writer(f) #csv.DictWriter(f, sorted(cashreg.keys()))
    # w.writerow(["date", "cash", "total_assets", "cash+assets"])
    w.writerow(["date", "cash", "unrealized_yield"])
    for key, value in sorted(cashreg.items()):
      w.writerow([key.date(), value['cash'], np.sum(value['cash'] + value['total_assets'])])
    print 'DEBUG: wrote report to:', outputfile
    # w.writerow(cashreg)
  # for day in sorted(cashreg.keys()):
    # print day, cashreg[day]['cash']
  # TO-DO: calculate unrealized value, and output it.

def output_performance_metrics():
  print 'DEBUG:', sys._getframe().f_code.co_name
  global cashreg
  # normalize prices times allocation
  # na_normalized_price = na_price / na_price[0, :] * allocation
  # cumulative return of total portfolio. Komt overeen met voorb. antw.
  # cumulative_return = np.nansum(na_normalized_price / na_normalized_price[0, :] * allocation, axis=1)
  # cumulative_return = som( (eindstand cash + assets) / beginstand portfolio)
  cumulative_return = []
  unrealized_return = []
  daily_return = []
  for key, value in sorted(cashreg.items()):
    unrealized_return.append((value['cash'] + value['total_assets']))
    cumulative_return.append((value['cash'] + value['total_assets']) / args.cash) # this is wrong, though it seems right for now.
  # for x in range(len(unrealized_return)):
  #   if x == 0:
  #     daily_return.append(0)
  #   daily_return.append((unrealized_return[x] - unrealized_return[x-1]) / unrealized_return[x])
  # is value total assets right? 5 days of the same cash + assets value couldn't be the case!!
  # for x in range(len(unrealized_return)):
  #   if x == 0:
  #     pass
  #   cumulative_return.append((unrealized_return[x] / unrealized_return[x -1]) -1)
  # cumulative_return[-1]
  # daily returns per dag, komt bijna overeen met voorbeeldantwoord.
  avg_daily_return = (cumulative_return[-1]) / len(cumulative_return)
  # volatility = standard deviation returns (per day)
  volatility = np.nanstd(cumulative_return)
  #
  # sharpe ratio: nearly right.
  # sqrt(252) = one year, daily sampling. 252/365.15 to get the sqrt() input value when using normal days (not trading days)
  sharpe = np.sqrt(252 / 365.15 * len(cumulative_return)) * avg_daily_return / volatility
  print 'INFO: orderfile: ' + args.orderfile
  print 'INFO: Start Date: ' + transactions.index.min().strftime("%B %d, %Y")
  print 'INFO: End Date: ' + transactions.index.max().strftime("%B %d, %Y")
  # print('Symbols: ' + str(ls_symbols))
  # print('Optimal Allocations: ' + str(results[4:,index_max_sharpe]))
  print 'INFO: Sharpe Ratio: ', sharpe
  print 'INFO: Volatility (stdev of daily returns): ', volatility
  print 'INFO: Average Daily Return: ', avg_daily_return
  print 'INFO: Cumulative Return:  ', cumulative_return[-1]

if __name__ == "__main__":
  args = arguments()
  read_orders(b_file=args.orderfile)
  setup_data_structures()
  if args.datasource == 'QSTK':
    parse_stockdata()
  elif args.datasource == 'quandl':
    download_stockdata()
  process_transactions()
  output_cashreg(outputfile=args.outputfile)
  output_performance_metrics()





# running it:
# python ./marketsim.py --cash 50000 --transactions ./eventstudy.csv --output ./eventstudyreport.csv --datasource=QSTK




