#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler as ep

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys
import pdb # python debugger

# print "Pandas Version", pd.__version__


def find_price_events(ls_symbols, dt_start, dt_end, valuation, c_dataobj=da.DataAccess('Yahoo', cachestalltime=0)):
  # We need closing prices so the timestamp should be hours=16.
  dt_timeofday = dt.timedelta(hours=16)
  # Get a list of trading days between the start and the end.
  ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
  # Keys to be read from the data, it is good to read everything in one go.
  ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
  # Reading the data, now d_data is a dictionary with the keys above.
  # Timestamps and symbols are the ones that were specified before.
  ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
  d_data = dict(zip(ls_keys, ldf_data))
  #
  # Filling the data for NAN
  for s_key in ls_keys:
    d_data[s_key] = d_data[s_key].fillna(method='ffill')
    d_data[s_key] = d_data[s_key].fillna(method='bfill')
    d_data[s_key] = d_data[s_key].fillna(1.0)
  df_events = d_data['actual_close'].copy()
  # effectively zero the values.
  df_events *= np.NAN
  #
  # get values from a certain column (key)
  # d_data['actual_close'].get('AES') # plus row num: [504]
  #
  for s_sym in ls_symbols:
    for day in xrange(1, len(ldt_timestamps)):
      # if yesterday's price >= $5 and if today's price < $5
      if d_data['actual_close'].get(s_sym)[day - 1] >= valuation and d_data['actual_close'].get(s_sym)[day] < valuation:
        # print( s_sym, 'yesterday"s price is ', d_data['actual_close'].get(s_sym)[day - 1], 'and today is.', d_data['actual_close'].get(s_sym)[day])
        # write 1 in df_events.
        df_events[s_sym][day] = 1
  # print "Creating pdf"
  # ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
  #               s_filename='MyEventStudy'+str(np.random.randint(1000))+'.pdf', b_market_neutral=True, b_errorbars=True,
  #               s_market_sym='SPY')
  return df_events



dt_start = dt.datetime(2008, 1, 1)
dt_end = dt.datetime(2009, 12, 31)
# Creating an object of the dataaccess class with Yahoo as the source.
c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
ls_symbols = c_dataobj.get_symbols_from_list("sp5002008")
ls_symbols.append('SPY')
# for valuation in [5.0, 6.0, 7.0, 8.0, 9.0, 10.0]:
#   df_events = find_price_events(ls_symbols, dt_start, dt_end, valuation)
#   print "For the",valuation,"event with the S&P500 in 2008, we find", int(df_events.sum().sum(axis=0)), "events. Data Range = " + dt_start.strftime("%B %d, %Y") + " to " + dt_end.strftime("%B %d, %Y")
#
#
# dt_start = dt.datetime(2012, 1, 1)
# dt_end = dt.datetime(2012, 12, 31)
# Creating an object of the dataaccess class with Yahoo as the source.
# c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
# ls_symbols = c_dataobj.get_symbols_from_list("sp5002012")
# ls_symbols.append('SPY')
# for valuation in [6.0, 7.0, 8.0, 9.0, 10.0]:
  # df_events = find_price_events(ls_symbols, dt_start, dt_end, valuation)
  # print "For the",valuation,"event with the S&P500 in 2012, we find", int(df_events.sum().sum(axis=0)), "events. Data Range = " + dt_start.strftime("%B %d, %Y") + " to " + dt_end.strftime("%B %d, %Y")#
#
#
#
#
# Part 3: Create your own event and experiment with it using the Event Profiler. Consider these questions:
# Idea: get into short positions on high market volatility.
# Is it possible to make money using your event?
# If it is possible, what investing strategy would you use? Think about details of entry (buy) and exit (sell), how many days would you hold?
# Is this a risky strategy?
# How much do you expect to make on each trade?
# How many times do you expect to be able to act on this opportunity each year?
# Is there some way to reduce the risk?

# c_dataobj=da.DataAccess('Yahoo', cachestalltime=0)
# We need closing prices so the timestamp should be hours=16.
dt_timeofday = dt.timedelta(hours=16)
# Get a list of trading days between the start and the end.
ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
# Keys to be read from the data, it is good to read everything in one go.
ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
# Reading the data, now d_data is a dictionary with the keys above.
# Timestamps and symbols are the ones that were specified before.
ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
d_data = dict(zip(ls_keys, ldf_data))

def find_stdev_events(stock_volume_deviation):
  #
  print "Filling the data for NAN"
  for s_key in ls_keys:
    d_data[s_key] = d_data[s_key].fillna(method='ffill')
    d_data[s_key] = d_data[s_key].fillna(method='bfill')
    d_data[s_key] = d_data[s_key].fillna(1.0)
  df_events = d_data['actual_close'].copy()
  # effectively zero the values.
  df_events *= np.NAN
  print "Loop through the symbols and days in a nested loop."
  for s_sym in ls_symbols:
    for day in xrange(1, len(ldt_timestamps)):
      pdb.set_trace
      # idea: if the stock volume (today) deviates X.X from a normal trading day (high volume) AND if the stock price (today) is lower than yesterday AND if the index (SPY) volatility is high.
      if day > 1 and (d_data['volume'].get(s_sym)[day] / d_data['volume'].get(s_sym).std()) > stock_volume_deviation \
        and d_data['actual_close'].get(s_sym)[day] < d_data['actual_close'].get(s_sym)[day -1] \
        and (d_data['volume'].get('SPY')[day] / d_data['volume'].get('SPY').std()) > 1.0:
        print s_sym, day #d_data['volume'].get(s_sym)[day] / d_data['volume'].get(s_sym).std()
        # write 1 in df_events.
        df_events[s_sym][day] = 1
        # if yesterday's price >= $5 and if today's price < $5
        # if d_data['actual_close'].get(s_sym)[day - 1] >= valuation and d_data['actual_close'].get(s_sym)[day] < valuation:
          # print( s_sym, 'yesterday"s price is ', d_data['actual_close'].get(s_sym)[day - 1], 'and today is.', d_data['actual_close'].get(s_sym)[day])
  print "Creating pdf"
  ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
    s_filename='MyEventStudy'+str(np.random.randint(1000))+'.pdf', b_market_neutral=True, b_errorbars=True,
    s_market_sym='SPY')
  return df_events

df_events = find_stdev_events(stock_volume_deviation=2)











# tutorial 2 example: when the stock drops 3% and the market goes up by 2%
# df_events = copy.deepcopy(df_close)
# df_events = df_events * np.NAN

# for s_sym in ls_symbols: # for each symbol
#   for i in range(1, len(ldt_timestamps)): # for each day
#     # Calculating the returns for this timestamp
#     f_symprice_today = df_close[s_sym].ix[ldt_timestamps[i]]
#     f_symprice_yest = df_close[s_sym].ix[ldt_timestamps[i - 1]]
#     f_marketprice_today = ts_market.ix[ldt_timestamps[i]]
#     f_marketprice_yest = ts_market.ix[ldt_timestamps[i - 1]]
#     f_symreturn_today = (f_symprice_today / f_symprice_yest) - 1
#     f_marketreturn_today = (f_marketprice_today / f_marketprice_yest) - 1
#     # Event is found if the symbol is down more then 3% while the
#     # market is up more then 2%
#     if f_symreturn_today <= -0.03 and f_marketreturn_today >= 0.02:
#        df_events[s_sym].ix[ldt_timestamps[i]] = 1

# references
# d_data.keys()
# d_data['low']
