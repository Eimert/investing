#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler as ep

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys, csv, argparse, os, cStringIO, quandl, logging
from scipy.ndimage.interpolation import shift

# Author      : Eimert Vink
# Created     : 21-10-2017
# Generation  : 2nd generation
# Description : computational investing coursera market simulator to analyze portfolio performance.
# Flow        : trade -> check price -> update ownership -> update cash.
# Input       : orderfile (.csv) format line: 2011,1,13,AAPL,Sell,1500, OR in generic csv format (quandl).
# Output      : stdout or csv with date and total value of portfolio using adjusted close prices.
# Usage       : see tail of this file for examples.
# Verification: The 1st and 2nd generation of this script deliver the same numbers. 2nd Gen code is refactored. Tested 2017-10-22 using orders.csv.

# Declare global variables
df_close = pd.DataFrame() # date, aapl, msft, cash ==1.0
df_cash = pd.DataFrame() # date, value=sum(price*hold)
df_trade = pd.DataFrame() # date, aapl, msft, cash
df_trade_matrix = pd.DataFrame() # date, aapl, msft, cash
df_holding_matrix = pd.DataFrame() # copy of trade_matrix
df_fund = pd.DataFrame() # holds the result
quandl.ApiConfig.api_key = '2GPdwxwGtTARrXz21oQR'

def setup_logging():
  # assuming loglevel is bound to the string value obtained from the command line argument. Convert to upper case to allow the user to specify --log=DEBUG or --log=debug
  numeric_level = getattr(logging, args.loglevel.upper(), None)
  if not isinstance(numeric_level, int):
      raise ValueError('Invalid log level: %s' % args.loglevel)
  # logging.basicConfig(filename='marketsim2.log', level=numeric_level)
  # logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  # logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  logging.basicConfig(level=numeric_level)
  logging.debug('logging started')

def is_valid_file(parser, arg):
    if not os.path.isfile(arg):
      parser.error("The file %s does not exist!" % arg)
    else:
      return arg

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        quit(2)

def arguments(args=None):
  parser = argparse.ArgumentParser(description='Market simulator that computes how a portfolio would perform. Input: 1. Amount of cash in $, 2. orders.csv (df_trade file), 3. output file for writing (values.csv)')
  parser.add_argument('--cash', type=int, action='store', dest='cash', required=True, help='an integer for the portfolio cash.')
  parser.add_argument('--transactions', action='store', dest='orderfile', metavar='FILE', required=True, help='Input transaction file, ex.: orders.csv', type=lambda x: is_valid_file(parser, x))
  parser.add_argument('--output', action='store', dest='outputfile', metavar='FILE', required=False, default='./report.csv', help='File for outputting the portfolio performance, ex.: values.csv')
  parser.add_argument('--datasource', action='store', dest='datasource', required=False, default='quandl', help='Specify the datasource: QSTK or quandl (default).')
  parser.add_argument('--close', action='store', dest='close', required=False, default='close', help='Specify the stock price type: actual_close or close (a.k.a. adjusted close, default).')
  parser.add_argument('--log', action='store', dest='loglevel', required=False, default='INFO', help='Specify the loglevel: [DEBUG, INFO (default), WARNING, ERROR, CRITICAL].')
  args = parser.parse_args()
  # print args.accumulate(args.integers)
  return args

def __unicode__(self):
  return unicode(self.some_field) or u''

def read_orders():
  """read inputfile into df_trade pd.DataFrame"""
  logging.info(sys._getframe().f_code.co_name)
  global df_trade
  columns = ['date', 'symbol', 'action', 'count']
  if 'event' not in args.orderfile: # QSTK
    df_trade= pd.read_csv(args.orderfile, header=None, parse_dates={'date': [0,1,2]}).dropna(axis=1)
  else:
    df_trade= pd.read_csv(args.orderfile, header=None, parse_dates={'date': [0]}).dropna(axis=1)   # 'normal' orderfile
  df_trade.columns = columns
  df_trade = df_trade.sort_values(by='date')
  df_trade = df_trade.set_index('date')

def input_verification():
  """checkout if the orders in this orderfile are supported."""
  logging.info(sys._getframe().f_code.co_name)
  # pandas will return Series object if there are multiple rows for a symbol on a certain day. Max. granularity == day, raise error if more than one trade of the same symbol on the same day. type(df_trade.loc['2011-08-01', 'symbol'])
  for day in sorted(df_trade.index): # currently fails on more than 1 trade per day.
    if not(isinstance(df_trade.loc[day, 'symbol'], str)): # yay, multiple values for this day
      if any(list(df_trade.loc[day, 'symbol'].duplicated(keep=False).values)):
        logging.warning('Cannot process more than one trade for a given symbol on a day.')
        logging.warning('duplicate value: ')
        logging.warning(str(df_trade.loc[day]))
        logging.warning('remove duplicate trade on this date in the transactions file: ' + str(args.orderfile))
        # exit(1)
  # to-do: warn if cash level is not sufficient to process an order.
  # to-do: warn if not all equities are sold at the end of execution. (realized <> unrealized)
  logging.debug('input orderfile ok.')

def setup_data_structures():
  logging.info(sys._getframe().f_code.co_name)
  global df_trade, df_trade_matrix, df_cash
  if args.datasource == 'QSTK':
    pass
    # print 'DEBUG: QSTK: not changing symbols from orderfile.'
  elif args.datasource == 'quandl':
    logging.debug('DEBUG: Changing GOOG into GOOGL')
    df_trade.replace('GOOG','GOOGL', inplace=True)
  else:
    logging.error('Unrecognized datasource given')
    exit(1)
  df_trade['symbol']
  d = {'date' : pd.date_range(start=df_trade.index.min(), end=df_trade.index.max())} #, freq='D'
  df_trade_matrix = pd.DataFrame(np.zeros((len(d['date']),len(set(df_trade['symbol'])))),columns=set(df_trade['symbol']),index=d.values())
  df_cash = pd.DataFrame(np.zeros((len(d['date']),1)),columns=['cash'],index=d.values())
  df_cash.set_value(df_cash.index.min(), 'cash', args.cash)

def download_stockdata():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata
  # using quandl: limited for anonymous users
  # http://help.quandl.com/article/206-how-do-i-download-a-specific-date-range-using-python
  # for each stock: download
  for symbol in df_trade['symbol'].unique():
    # stockdata.append(quandl.get("WIKI/" + symbol, start_date=df_trade.index.min(), end_date=df_trade.index.max()))
    data = quandl.get("WIKI/" + symbol, start_date=df_trade.index.min(), end_date=df_trade.index.max())['Adj. Close']
    assert(len(data) != 0), "quandl did return an empty dataset"
    stockdata.insert(loc=len(stockdata.keys()), column=symbol, value=data)

def parse_stockdata():
  logging.info(sys._getframe().f_code.co_name)
  global df_close
  # use QSTK files
  # Creating an object of the dataaccess class with Yahoo as the source.
  c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
  ls_symbols = c_dataobj.get_symbols_from_list("sp5002012")
  dt_start = df_trade.index.min()
  dt_end = df_trade.index.max() + dt.timedelta(days=1)
  # We need closing prices so the timestamp should be hours=16.
  dt_timeofday = dt.timedelta(hours=16)
  # Get a list of trading days between the start and the end.
  ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
  # Keys to be read from the data, it is good to read everything in one go.
  # ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close'] # adj_close == close
  ls_keys = [ args.close ]
  df_close = c_dataobj.get_data(ldt_timestamps, list(df_trade.symbol.unique()), ls_keys)[0] #+ ['\$SPX']
  # alert when a symbol could not be found
  for sym in df_close.keys():
    if df_close[sym].isnull().values.any():
      logging.warning('Data for one or more symbol(s) not found or incomplete. ' + str(sym))
      # quit(1)
  # timestamps are now in hours == 16.00, whereas df_trade expect hours == 00.00. changing it.
  # stockdata.index -= pd.DateOffset(hours=dt_timeofday)
  df_close.index -= dt_timeofday

def process_trade(day, cur_sym, whole_number):
  logging.debug(sys._getframe().f_code.co_name)
  global df_trade_matrix, df_cash
  add = float(df_trade_matrix.at[day, cur_sym] + whole_number)
  if df_trade_matrix.at[day, cur_sym] <> 0.0 : logging.warning('Updating df_trade_matrix. %s %s \t n-1: \t %d \t n: \t %d \t n+1: \t %d' % (day.strftime('%Y-%m-%d'), cur_sym, df_trade_matrix.at[day, cur_sym], whole_number, add))
  df_trade_matrix.set_value(day, cur_sym, add)
  price = df_close[cur_sym][day] # lookup price
  transaction_sum = -(price * whole_number)
  # get previous df_cash value, add transaction sum, then write.
  df_cash.set_value(day, 'cash', df_cash.at[day, 'cash'] + transaction_sum)

def process_df_trade():
  logging.info(sys._getframe().f_code.co_name)
  global df_trade, df_cash
  logging.info('filling df_trade_matrix with whole number.')
  for day in sorted(df_trade_matrix.index):
    if day in df_trade.index:
      # not pretty code with repetition, because there can be multiple df_trade on one day.
      if isinstance(df_trade.ix[day]['action'], str): # set variables
        cur_sym = df_trade.ix[day]['symbol']
        action = df_trade.ix[day]['action']
        order_count = df_trade.ix[day]['count']
        whole_number = -order_count if action == 'Sell' else order_count
        logging.debug('day: %s cur_sym: %s \t action: %s \t %d \t whole_num: %d'% (day.strftime('%Y-%m-%d'), cur_sym, action, order_count, whole_number))#,'total sum:', transaction_sum
        process_trade(day=day, cur_sym=cur_sym, whole_number=whole_number)
      else: #type is pandas.core.series.Series
        # in case there are multiple, iterate over the number of df_trade that day
        for num in range(len(df_trade.ix[day].values)):
          cur_sym = df_trade.ix[day]['symbol'][num]
          action = df_trade.ix[day]['action'][num]
          order_count = df_trade.ix[day]['count'][num]
          whole_number = -order_count if action == 'Sell' else order_count
          logging.debug('day: %s cur_sym: %s \t action: %s \t %d \t whole_num: %d'% (day.strftime('%Y-%m-%d'), cur_sym, action, order_count, whole_number))#,'total sum:', transaction_sum
          process_trade(day=day, cur_sym=cur_sym, whole_number=whole_number)


def step_5():
  logging.info(sys._getframe().f_code.co_name)
  """This is where the magic happens."""
  global df_trade_matrix, df_close, df_holding_matrix, df_fund
  df_close['_CASH'] = 1.0
  df_trade_matrix['_CASH'] = df_cash
  df_holding_matrix = df_trade_matrix.cumsum() # sum up the values of the columns.
  #
  # climax
  #
  # Cash value is realized yield.
  df_fund = df_close * df_holding_matrix # multiply the price of the equities by the number of shares purchased.
  # Cash value is unrealized yield (more fluid):
  df_fund['unrealized_yield'] = df_fund.cumsum(axis=1)['_CASH']
  logging.info('Created df_fund with an overview of the performance.')

def output_fund():
  logging.info(sys._getframe().f_code.co_name)
  with open(args.outputfile, 'wb') as f:
    w = csv.writer(f) #csv.DictWriter(f, sorted(cashreg.keys()))
    w.writerow(["date", "cash", "unrealized_yield"])
    for day in sorted(df_fund.index):
      w.writerow([day.date(), float('%.2f' % df_fund.at[day, '_CASH']), float('%.2f' % df_fund.at[day, 'unrealized_yield'])])
    logging.debug('wrote report to: ' + str(args.outputfile))

def output_performance_metrics():
  logging.info(sys._getframe().f_code.co_name)
  global df_fund
  # avg daily return based on unrealized_yield nan values are wrong, because 'mondays' are not calculated.
  df_fund['daily_return'] = df_fund['unrealized_yield'].dropna() / df_fund['unrealized_yield'].dropna().shift(1) -1
  # daily returns per day
  avg_daily_return = np.nanmean(df_fund['daily_return'])
  # avg_daily_return_spx =
  # volatility = standard deviation returns (per day)
  volatility = np.nanstd(df_fund['daily_return'])
  # sharpe ratio
  # sqrt(252) = one year, daily sampling. 252/365.15 to get the sqrt() input value when using normal days (not trading days)
  # sharpe = np.sqrt(252 / 365.15 * len(df_fund.index)) * avg_daily_return / volatility
  sharpe = np.sqrt(252) * avg_daily_return / volatility
  logging.info('The final value of the portfolio is: %.0f' % df_fund.at[df_fund.index.max(), 'unrealized_yield'])
  logging.info('Details of the Performance of the portfolio')
  logging.info('Orderfile: ' + args.orderfile)
  logging.info('Price type: ' + args.close)
  # logging.info('Data range: ' + df_trade.index.min().strftime("%B %d, %Y") + ' to: ' + df_trade.index.max().strftime("%B %d, %Y"))
  logging.info('Data range: ' + str(df_trade.index.min().date()) + ' to ' + str(df_trade.index.max().date()))
  # print('Symbols: ' + str(ls_symbols))
  # print('Optimal Allocations: ' + str(results[4:,index_max_sharpe]))
  logging.info('Sharpe Ratio of fund: %.14f' % sharpe)
  logging.info('Total return of fund: %.6f' % (df_fund['unrealized_yield'][-1] / df_fund['unrealized_yield'][0]))
  # logging.info('Sharpe Ratio of $SPX: %.2f' % sharpe_spx)
  logging.info('Volatility (stdev of daily return) of fund: %.14f' % volatility)
  logging.info('Average Daily Return of fund: %.14f' % avg_daily_return)

# na_price = d_data['close'].values
# plt.clf()
# plt.plot(ldt_timestamps, na_price)
# plt.legend(ls_symbols)
# plt.ylabel('Adjusted Close')
# plt.xlabel('Date')
# plt.savefig('adjustedclose.pdf', format='pdf')

if __name__ == "__main__":
  args = arguments()
  setup_logging()
  read_orders()
  input_verification()
  setup_data_structures()
  if args.datasource == 'QSTK':
    parse_stockdata()
  elif args.datasource == 'quandl':
    download_stockdata()
  process_df_trade()
  step_5()
  output_fund()
  output_performance_metrics()

# running it:
# python ./marketsim2.py --cash 50000 --transactions ./eventstudy.csv --output ./eventstudyreport.csv --datasource=QSTK
# python -i ./marketsim2.py --cash 50000 --transactions ./orders2.csv --output ./report2.csv --datasource=QSTK --log info
# python -i ./marketsim2.py --cash 1000000 --transactions ./orders.csv --output ./report2.csv --datasource=QSTK
# python ./marketsim2.py --cash 100000 --transactions eventstudy-bollinger-bands2.csv --output ./report-eventstudy-bollinger-bands2.csv --datasource=QSTK


# select between two dates:     df_trade_matrix.loc['2011-07-22':'2011-08-22']
# get cell value:               df_trade_matrix.at[day, cur_sym]
#                               df_fund.at[pd.to_datetime('2009-12-14'), 'MSI']



