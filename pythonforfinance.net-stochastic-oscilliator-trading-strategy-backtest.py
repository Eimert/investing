#import relevant modules
import pandas as pd
import numpy as np
from pandas_datareader import data
import matplotlib.pyplot as plt

df = data.DataReader('AAPL', 'yahoo',start='1/1/2010')

df['L14'] = df['Low'].rolling(window=14).min()
df['H14'] = df['High'].rolling(window=14).max()

df['%K'] = 100*((df['Close'] - df['L14']) / (df['H14'] - df['L14']) )
df['%D'] = df['%K'].rolling(window=3).mean()

df['Sell Entry'] = ((df['%K'] &lt; df['%D']) &amp; (df['%K'].shift(1) &gt; df['%D'].shift(1))) &amp; (df['%D'] &gt; 80)
df['Buy Entry'] = ((df['%K'] &gt; df['%D']) &amp; (df['%K'].shift(1) &lt; df['%D'].shift(1))) &amp; (df['%D'] &lt; 20)

#Create empty "Position" column
df['Position'] = np.nan

#Set position to -1 for sell signals
df.loc[df['Sell Entry'],'Position'] = -1

#Set position to -1 for buy signals
df.loc[df['Buy Entry'],'Position'] = 1

#Set starting position to flat (i.e. 0)
df['Position'].iloc[0] = 0

#Forward fill the position column to show holding of positions through time
df['Position'] = df['Position'].fillna(method='ffill')

#Set up a column holding the daily Apple returns
df['Market Returns'] = df['Close'].pct_change()

#Create column for Strategy Returns by multiplying the daily Apple returns by the position that was held at close
#of business the previous day
df['Strategy Returns'] = df['Market Returns'] * df['Position'].shift(1)

#Finally plot the strategy returns versus Apple returns
df[['Strategy Returns','Market Returns']].cumsum().plot(figsize=(20,10))
