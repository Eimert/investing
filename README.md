# investing
My tooling from the computational-investing coursera course. Made accessible through Flask with Swagger UI, running in a Docker container.

### Getting started
```
git clone
eimert@EIM investing $ . ./flask/local/bin/activate
pip install -r requirements.txt
```
[Change QSTK library](https://github.com/QuantSoftware/QuantSoftwareToolkit/pull/65/files)

### Run!
```
python ./marketsim3.py
```
