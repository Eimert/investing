#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler as ep

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys, csv, argparse, os, cStringIO, quandl, logging

# Description
# produces bollinger bands technical analysis for a given ticker symbol.
# flow: run bollinger-bands.py, open generated pdf
# output: pdf with bollinger band for symbol in the given timeframe.

# Declare global variables
stockdata = dict()
df = pd.DataFrame()                  # structure: stockdata[ls_keys][symbol]
c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
ls_symbols = c_dataobj.get_symbols_from_list("sp5002012")
numsd = 1 # number of times to multiply the standard deviation in the bollinger bands calculation

def setup_logging():
  # assuming loglevel is bound to the string value obtained from the command line argument. Convert to upper case to allow the user to specify --log=DEBUG or --log=debug
  numeric_level = getattr(logging, args.loglevel.upper(), None)
  if not isinstance(numeric_level, int):
      raise ValueError('Invalid log level: %s' % args.loglevel)
  # logging.basicConfig(filename='marketsim2.log', level=numeric_level)
  # logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  # logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  logging.basicConfig(level=numeric_level)
  logging.debug('logging started')

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def arguments(args=None):
  parser = argparse.ArgumentParser(description='Bollinger Bands. Provide ticker symbol, timerange and # lookback days.')
  parser.add_argument('--symbol', type=str, action='store', dest='symbol', required=True, help='Symbol to analyze.')
  parser.add_argument('--start', type=lambda d: dt.datetime.strptime(d, '%d%m%Y').date(), action='store', dest='dt_start', required=True, help='start date: DDMMYYYY')
  parser.add_argument('--end', type=lambda d: dt.datetime.strptime(d, '%d%m%Y').date(), action='store', dest='dt_end', required=True, help='end date: DDMMYYYY')
  parser.add_argument('--lookback', type=int, action='store', dest='lookback', required=False, default=20, help='Look back this number of days in the bollinger band calculation.')
  parser.add_argument('--report', action='store', dest='reportfile', metavar='FILE', required=False, help='Write visual output to this file.')
  parser.add_argument('--log', action='store', dest='loglevel', required=False, default='INFO', help='Specify the loglevel: [DEBUG, INFO (default), WARNING, ERROR, CRITICAL].')
  # parser.add_argument('--datasource', action='store', dest='datasource', required=False, default='quandl', help='Specify the datasource: QSTK or quandl.')
  args = parser.parse_args()
  return args

def parse_stockdata():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata, c_dataob, ls_symbols
  # use QSTK files
  # Creating an object of the dataaccess class with Yahoo as the source.
  dt_start = args.dt_start
  dt_end = args.dt_end # + dt.timedelta(days=1)
  # We need closing prices so the timestamp should be hours=16.
  dt_timeofday = dt.timedelta(hours=16)
  # Get a list of trading days between the start and the end.
  ldt_timestamps = du.getNYSEdays(args.dt_start, args.dt_end, dt_timeofday)
  # Keys to be read from the data, it is good to read everything in one go.
  ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
  # ls_keys = ['actual_close']
  # ls_symbols.append('SPY') # ignore
  # stockdata = c_dataobj.get_data(ldt_timestamps, [ args.symbol ], ls_keys) # you sure about 0?
  # stockdata = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
  # d_data = dict(zip(ls_keys, stockdata))
  l_data = c_dataobj.get_data(ldt_timestamps, [ args.symbol ], ls_keys)
  stockdata = dict(zip(ls_keys, l_data))
  # to-do: alert when a symbol could not be found
  # timestamps are now in hours == 16.00, whereas transactions expect hours == 00.00. changing it.
  # stockdata.index -= pd.DateOffset(hours=dt_timeofday)
  # stockdata.index -= dt_timeofday

def bollinger():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata
  # get rolling standard deviation over this lookback period
  stockdata['stdev'] = stockdata['close'][args.symbol].rolling(window=args.lookback).std()
  # upper/lower band: avg price over this window + or - avg standard deviation.
  # stockdata['upper'] = stockdata['low'].rolling(window=args.lookback).min()
  # stockdata['lower'] = stockdata['high'].rolling(window=args.lookback).max()
  stockdata['upper'] = stockdata['close'][args.symbol].rolling(window=args.lookback).mean() + (numsd * stockdata['stdev'])
  stockdata['lower'] = stockdata['close'][args.symbol].rolling(window=args.lookback).mean() - (numsd * stockdata['stdev'])
  # define middle.
  stockdata['rolling mean'] = stockdata['close'][args.symbol].rolling(window=args.lookback).mean()
  # part 2: bollinger boolean val
  # Bollinger_val = (price - rolling_mean) / (rolling_std)
  stockdata['bollinger val'] = (stockdata['close'][args.symbol] - stockdata['rolling mean']) / stockdata['stdev']
  # stockdata['positivismo'] = np.nan

def fill_df():
  logging.info(sys._getframe().f_code.co_name)
  global df, stockdata
  # to-do: stop manging stockdata['close'].keys()
  df = pd.DataFrame(stockdata['close']) # creates the df with datetimeindex
  # del df[args.symbol] # remove that undescriptive named column
  df['close'] = pd.DataFrame(stockdata['close'])  # alters the dict as well.. :(
  df['upper'] = pd.DataFrame(stockdata['upper'])
  df['lower'] = pd.DataFrame(stockdata['lower'])
  df['rolling mean'] = pd.DataFrame(stockdata['rolling mean'])
  # part 2: Boolean indicator.
  df['bollinger val'] = pd.DataFrame(stockdata['bollinger val'])
  # df.loc[df['bollinger val'] > 1,'positivismo'] = 1
  # df.loc[df['bollinger val'] < -1,'negativismo'] = -1

def output():
  logging.info(sys._getframe().f_code.co_name)
  plt.clf()
  ax = df['close'].plot(title=args.symbol + ' Bollinger Bands',label=args.symbol,color='red',figsize=(20,10))
  df['rolling mean'].plot(ax=ax, color='gold')
  # df['close'].plot(ax=ax, color = 'red')
  df['upper'].plot(ax=ax, color = 'cyan')
  df['lower'].plot(ax=ax, color = 'cyan')
  # df['positivismo'].plot(color = 'green')
  # df['negativismo'].plot(color = 'red')
  ax.set_xlabel("Date")
  ax.set_ylabel("Adjusted close")
  ax.legend(loc='upper left', prop={'size':8})
  # stockdata.plot(y=[args.symbol,'stdev', 'upper', 'lower'], title='Bollinger Bands')
  # stockdata['close'].cumsum().plot(figsize=(20,10))
  # stockdata['upper'].plot(figsize=(20,10), linewidth=2)
  # df['close'].plot(figsize=(20,10), linewidth=2, color = 'red')
  # stockdata['lower'].plot(figsize=(20,10), linewidth=2)
  # stockdata['close'].plot(figsize=(20,10), linewidth=3)
  # plt.title(args.symbol + ' close price')
  # plt.xlabel('day')
  # plt.ylabel('price')
  # plt.show()
  if args.reportfile:
    plt.savefig(args.reportfile + '.svg', format='svg')
    logging.info('wrote report to: ' + str(args.reportfile) + '.svg')
  else:
    plt.savefig(args.symbol + '_bollinger.svg', format='svg')
    logging.info('wrote report to: ' + args.symbol + '_bollinger.svg')

if __name__ == "__main__":
  args = arguments()
  setup_logging()
  parse_stockdata()
  bollinger()
  fill_df()
  output()


# running it:
# python -i ./bollinger-bands.py --symbol AAPL --start 01122010 --end 01012011 --lookback 20 --report bollinger
# python ./bollinger-bands.py --symbol GOOG --start 01012010 --end 31122010

















