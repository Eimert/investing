#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys

# pprint "Pandas Version", pd.__version__

# def simulate(dt_start, dt_end, ls_symbols):
# input:
# start 	datetime.date
# end 	    datetime.date
# tickers	list of ticker symbols, ex ['GOOG', 'AAPL']
# function will calculate the best allocation (with the highest sharpe ratio)
# allocation = [0.0,0.0,0.0,1.0]
#
# print('Usage: start=datetime.date end=datetime.date tickers=list')
# print('Example: dt.datetime(2011, 1, 1) dt.datetime(2011, 12, 31) ["AXP", "HPQ", "IBM", "HNZ"]')
# testing
dt_start = dt.datetime(2010, 1, 1)
dt_end = dt.datetime(2010, 12, 31)
# ls_symbols = ['AAPL', 'GLD', 'GOOG', 'XOM']
ls_symbols = ['BRCM', 'TXN', 'IBM', 'HNZ']
# dt_start = sys.argv[1]
# dt_end = sys.argv[2]
# dt_symbols = sys.argv[3]
#
# We need closing prices so the timestamp should be hours=16.
dt_timeofday = dt.timedelta(hours=16)
# Get a list of trading days between the start and the end.
ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
# Creating an object of the dataaccess class with Yahoo as the source.
c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
# Keys to be read from the data, it is good to read everything in one go.
ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
# Reading the data, now d_data is a dictionary with the keys above.
# Timestamps and symbols are the ones that were specified before.
ldf_data = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
d_data = dict(zip(ls_keys, ldf_data))
# Filling the data for NAN
for s_key in ls_keys:
    d_data[s_key] = d_data[s_key].fillna(method='ffill')
    d_data[s_key] = d_data[s_key].fillna(method='bfill')
    d_data[s_key] = d_data[s_key].fillna(1.0)
# Getting the numpy ndarray of close prices.
na_price = d_data['close'].values
#
portfolio_runs = 5000
# create array to hold results
results = np.zeros((4+len(ls_symbols),portfolio_runs))
# allocations = [[0.2, 0.2, 0.4, 0.2], [0.2, 0.0, 0.0, 0.8], [0.0, 0.0, 1.0, 0.0], [0.4, 0.6, 0.0, 0.0]]
for y in xrange(portfolio_runs):
# for y in xrange(len(allocations)):
    # pick # of ls_symbols random numbers
    allocation = np.random.random(len(ls_symbols))
    # divide.by itself, so the allocation add up to 1.0
    # wei / np.sum(wei)
    allocation /= np.sum(allocation)
    # static allocations
    # allocation = allocations[y]
    # print(allocation)
    #
    # fixed allocation
    # normalize prices times allocation
    na_normalized_price = na_price / na_price[0, :] * allocation
    #
    #
    # daily returns per dag, komt bijna overeen met voorbeeldantwoord.
    avg_daily_return = np.nanmean((na_normalized_price[1:] / na_normalized_price[:-1]) -1)
    # cumulative return of total portfolio. Komt overeen met voorb. antw.
    cumulative_return = np.nansum(na_normalized_price / na_normalized_price[0, :] * allocation, axis=1)
    # np.sum(na_price / na_price[0, :] * allocation, axis=1)
    np.sum(na_price / na_price[0, :] * allocation, axis=1)
    # returns in percentage each day (251)
    (na_price[1:] / na_price[:-1]) -1
    # Sum each row for each day. That is your cumulative daily portfolio value.
    np.sum(na_price / na_price[0, :] * allocation, axis=1)
    # volatility = standard deviation returns (per day)
    volatility = np.nanstd(cumulative_return[1:] / cumulative_return[:-1])
    # sharpe ratio: nearly right.
    sharpe = np.sqrt(len(na_normalized_price)) * avg_daily_return / volatility
    # store results
    # print(avg_daily_return, volatility, sharpe)
    # results[x,y]
    results[0,y] = cumulative_return[-1]
    results[1,y] = volatility
    results[2,y] = sharpe
    results[3,y] = avg_daily_return
    # iterate through the allocation list and save data in the results array
    for x in range(len(allocation)):
        # print(x+3, y, allocation)
        results[x+4,y] = allocation[x]
#
# find max sharpe ratio, and print allocation details.
# unpack the list
# xmax, ymax = results.max(axis=0)[2]
# find index
index_max_sharpe = results.argmax(axis=1)[2]
# print values of index...
# show all sharpe ratios: results[2,]
#
print('Start Date: ' + dt_start.strftime("%B %d, %Y"))
print('End Date: ' + dt_end.strftime("%B %d, %Y"))
print('Symbols: ' + str(ls_symbols))
print('Optimal Allocations: ' + str(results[4:,index_max_sharpe]))
print('Sharpe Ratio: ' + str(results[2,index_max_sharpe]))
print('Volatility (stdev of daily returns): ' + str(results[1,index_max_sharpe]))
print('Average Daily Return: ' + str(results[3,index_max_sharpe]))
print('Cumulative Return:  ' + str(results[0,index_max_sharpe]))
#
# print('Start Date: ' + dt_start.strftime("%B %d, %Y"))
# print('End Date: ' + dt_end.strftime("%B %d, %Y"))
# print('Symbols: ' + str(ls_symbols))
# print('Optimal Allocations: ' + str(allocation))
# print('Sharpe Ratio: ' + str(sharpe))
# print('Volatility (stdev of daily returns): ' + str(volatility))
# print('Average Daily Return: ' + str(avg_daily_return))
# print('Cumulative Return:  ' + str(cumulative_return[-1]))

# if __name__ == '__main__':
  # simulate(dt.datetime(2011, 1, 1), dt.datetime(2011, 12, 31), ["AXP", "HPQ", "IBM", "HNZ"])


#convert results array to Pandas DataFrame
results_frame = pd.DataFrame(results.T,columns=['ret','stdev','sharpe','daily_ret',ls_symbols[0],ls_symbols[1],ls_symbols[2],ls_symbols[3]])

#locate position of portfolio with highest Sharpe Ratio
max_sharpe_port = results_frame.iloc[results_frame['sharpe'].idxmax()]
#locate positon of portfolio with minimum standard deviation
min_vol_port = results_frame.iloc[results_frame['stdev'].idxmin()]

#create scatter plot coloured by Sharpe Ratio
plt.scatter(results_frame.stdev,results_frame.ret,c=results_frame.sharpe,cmap='RdYlBu')
plt.xlabel('Volatility')
plt.ylabel('Returns')
plt.colorbar()
#plot red star to highlight position of portfolio with highest Sharpe Ratio
plt.scatter(max_sharpe_port[1],max_sharpe_port[0],marker=(5,1,0),color='r',s=1000)
#plot green star to highlight position of minimum variance portfolio
plt.scatter(min_vol_port[1],min_vol_port[0],marker=(5,1,0),color='g',s=1000)
# plt.show()
