#!/usr/bin/python
# QSTK Imports
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkstudy.EventProfiler as ep

# Third Party Imports
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import sys, csv, argparse, os, cStringIO, quandl, logging

# Description
# event analysis tool of True/False matrix with buy/sell stock events.
# flow: run event analysis tool, feed spreadsheet to marketsim.py.
# output: stdout or csv with date and total value of portfolio using adjusted close prices.

# Declare global variables
stockdata = dict()
events = pd.DataFrame()              # stucture: df_events[symbol][day] = True/False (event happended)
buy_amount = 100        # amount of shares to buy when the event happens
# sell_after = dt.timedelta(days=5)   # sell the stocks # trading days later.
c_dataobj = da.DataAccess('Yahoo', cachestalltime=0)
ls_symbols = c_dataobj.get_symbols_from_list("sp5002012")
numsd = 1 # number of times to multiply the standard deviation in the bollinger bands calculation

def setup_logging():
  # assuming loglevel is bound to the string value obtained from the command line argument. Convert to upper case to allow the user to specify --log=DEBUG or --log=debug
  numeric_level = getattr(logging, args.loglevel.upper(), None)
  if not isinstance(numeric_level, int):
      raise ValueError('Invalid log level: %s' % args.loglevel)
  # logging.basicConfig(filename='marketsim2.log', level=numeric_level)
  # logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  # logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', datefmt='%d-%m-%Y %H:%M:%S', level=numeric_level)
  logging.basicConfig(level=numeric_level)
  logging.debug('logging started')

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def arguments(args=None):
  parser = argparse.ArgumentParser(description='Event study. Define an event, and compute past returns. Symbol list = S&P 500 of 2012. * At every event Buy 100 shares of the equity, and Sell them 5 trading days later. In case not enough days are available Sell them on the last trading day. (Similar to what the homework 4 description wanted).')
  parser.add_argument('--start', type=lambda d: dt.datetime.strptime(d, '%d%m%Y').date(), action='store', dest='dt_start', default='01122008', required=False, help='start date: DDMMYYYY')
  parser.add_argument('--end', type=lambda d: dt.datetime.strptime(d, '%d%m%Y').date(), action='store', dest='dt_end', default='31122009', required=False, help='end date: DDMMYYYY')
  parser.add_argument('--actual-close-price', type=float, action='store', dest='actual_close_price', required=False, help='The event is defined as when the actual close of the stock price drops below this value (float, for example: 9.00).')
  parser.add_argument('--sell-after', type=int, action='store', dest='sell_after', required=False, default=dt.timedelta(days=5), help='Sell after this number of days.')
  parser.add_argument('--bollinger-bands', nargs="*", action='store', dest='bollinger_bands', required=False, help='List : [Bollinger value of equity today < -2.0, Bollinger value of equity yesterday >= -2.0, Bollinger value of SPY today >= 1.1]. Example input: -2.0 -2.0 -1.1')
  parser.add_argument('--lookback', type=int, action='store', dest='lookback', required=False, default=20, help='Look back this number of days in the bollinger band calculation.')
  parser.add_argument('--output', action='store', dest='outputfile', metavar='FILE', required=False, default='./eventstudy.csv', help='Write transactions to this file. Use as input for the market simulator.')
  # feature_parser = parser.add_mutually_exclusive_group(required=False)
  # parser.add_argument('--pdf', action='store_true', dest='pdf', help='Output PDF report true/false.')
  parser.add_argument('--pdf', action='store', dest='pdf', help='Output PDF report true/false.')
  # parser.add_argument('--no-pdf', action='store_false', dest='pdf', help='Output PDF report true/false.')
  # parser.set_defaults(pdf=False)
  # parser.add_argument('--datasource', action='store', dest='datasource', required=False, default='quandl', help='Specify the datasource: QSTK or quandl.')
  parser.add_argument('--log', action='store', dest='loglevel', required=False, default='INFO', help='Specify the loglevel: [DEBUG, INFO (default), WARNING, ERROR, CRITICAL].')
  args = parser.parse_args()
  # print args.accumulate(args.integers)
  return args

def parse_stockdata():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata
  # use QSTK files
  # Creating an object of the dataaccess class with Yahoo as the source.
  dt_start = args.dt_start
  dt_end = args.dt_end # + dt.timedelta(days=1)
  # We need closing prices so the timestamp should be hours=16.
  dt_timeofday = dt.timedelta(hours=16)
  # Get a list of trading days between the start and the end.
  ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
  # Keys to be read from the data, it is good to read everything in one go.
  # ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
  ls_keys = ['close']
  ls_symbols.append('SPY')
  stockdata = c_dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)[0]
  # to-do: alert when a symbol could not be found
  # timestamps are now in hours == 16.00, whereas transactions expect hours == 00.00. changing it.
  # stockdata.index -= pd.DateOffset(hours=dt_timeofday)
  stockdata.index -= dt_timeofday

def setup_data_structures():
  logging.info(sys._getframe().f_code.co_name)
  global events, stockdata
  events = stockdata.copy()
  # effectively zero the values.
  events *= np.NAN
  # prepare stockdata for bollinger
  for key in ['stdev', 'upper', 'lower', 'rolling mean', 'bollinger val']:
    stockdata[key] = np.nan

def find_price_events():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata, df_events, c_dataob, ls_symbols
  # iterate over the symbols and dates, and look for events on every trading day.
  for s_sym in ls_symbols:
    for day in range(1, len(stockdata)):
      # if yesterday's price >= $5 and if today's price < $5
      # if d_data['actual_close'].get(s_sym)[day - 1] >= args.actual_close_price and d_data['actual_close'].get(s_sym)[day] < args.actual_close_price:
      if stockdata[s_sym][day - 1] >= args.actual_close_price and stockdata[s_sym][day] < args.actual_close_price:
        logging.debug(s_sym, 'yesterday\'s price is ', stockdata[s_sym][day - 1], 'and today is.', stockdata[s_sym][day], ' event == True')
        # write 1 in df_events.
        events[s_sym][day] = 1
  # print "Creating pdf"
  # ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
  #               s_filename='MyEventStudy'+str(np.random.randint(1000))+'.pdf', b_market_neutral=True, b_errorbars=True,
  #               s_market_sym='SPY')

def find_bollinger_band_events():
  logging.info(sys._getframe().f_code.co_name)
  global stockdata
  # iterate over the symbols and dates, and look for events on every trading day.
  # for s_sym in ls_symbols:
  # first calculate for SPY, then for the other symbols
  for s_sym in ['SPY'] + ls_symbols:
    # bollinger function for this symbol
    #
    # get rolling standard deviation over this lookback period
    stockdata['stdev'] = stockdata[s_sym].rolling(window=args.lookback).std()
    # upper/lower band: avg price over this window + or - avg standard deviation.
    # stockdata['upper'] = stockdata['low'].rolling(window=args.lookback).min()
    # stockdata['lower'] = stockdata['high'].rolling(window=args.lookback).max()
    stockdata['upper'] = stockdata[s_sym].rolling(window=args.lookback).mean() + (numsd * stockdata['stdev'])
    stockdata['lower'] = stockdata[s_sym].rolling(window=args.lookback).mean() - (numsd * stockdata['stdev'])
    # define middle.
    stockdata['rolling mean'] = stockdata[s_sym].rolling(window=args.lookback).mean()
    # part 2: bollinger boolean val
    # Bollinger_val = (price - rolling_mean) / (rolling_std)
    stockdata['bollinger val'] = (stockdata[s_sym] - stockdata['rolling mean']) / stockdata['stdev']
    # stockdata['positivismo'] = np.nan
    #
    if s_sym == 'SPY':
      stockdata['spy bollinger val'] = stockdata['bollinger val']
      continue
    # analyze bollinger data for symbol s_sym
    for day in range(1, len(stockdata)):
      # the event is defined as when:
      # Bollinger value of equity today < -2.0
      # Bollinger value of equity yesterday >= -2.0
      # Bollinger value of SPY today >= 1.1
      if stockdata['bollinger val'][day] < float(args.bollinger_bands[0]) and \
      stockdata['bollinger val'][day - 1] >= float(args.bollinger_bands[1]) and \
      stockdata['spy bollinger val'][day] >= float(args.bollinger_bands[2]):
        # print stockdata['bollinger val'][day], '<', float(args.bollinger_bands[0]), stockdata['bollinger val'][day] < float(args.bollinger_bands[0]), stockdata['bollinger val'][day - 1], '>=', float(args.bollinger_bands[1]), stockdata['bollinger val'][day - 1] >= float(args.bollinger_bands[1]), stockdata['spy bollinger val'][day], '>=', float(args.bollinger_bands[2]), stockdata['spy bollinger val'][day] >= float(args.bollinger_bands[2])
        logging.debug('daynum', day, s_sym, 'event == True')
        # write 1 in df_events.
        events[s_sym][day] = 1
  logging.info('# events found: ' + str(events.count().sum()))

def output_events():
  logging.info(sys._getframe().f_code.co_name)
  with open(args.outputfile, 'wb') as f:
    w = csv.writer(f) #csv.DictWriter(f, sorted(cashreg.keys()))
    # w.writerow(["date", "symbol", "action", "count"])   2011,1,10,AAPL,Buy,1500,
    # iterate over 1. dates and 2. symbols. Write buy and sell row when event == 1.
    for index, row in events.iterrows():
      # every day: iterate over symbols
      # print index
      for sym in row.keys():
        # print sym
        # print row[sym] ## value
        if row[sym] == 1:
          logging.debug(index.date(), 'buy:', sym)
          selldate = args.sell_after
          while (index.date() + selldate not in events.index):
            selldate -= dt.timedelta(days=1)
          logging.debug(index.date() + selldate, 'sell:', sym)
          # write
          w.writerow([index.date(), sym, 'Buy', buy_amount])
          w.writerow([index.date() + selldate, sym, 'Sell', buy_amount])
    logging.info('wrote output orderfile to: ' + str(args.outputfile))

def output_pdf():
  logging.info(sys._getframe().f_code.co_name)
  global events, ls_symbols
  dt_start = args.dt_start
  dt_end = args.dt_end
  ldt_timestamps = du.getNYSEdays( dt_start, dt_end, dt.timedelta(hours=16) )
  dataobj = da.DataAccess('Yahoo')
  ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
  ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
  d_data = dict(zip(ls_keys, ldf_data))
  ep.eventprofiler(events, d_data,i_lookback=20,i_lookforward=20,s_filename=str(args.outputfile)+'.pdf')
  logging.info('wrote pdf report to: ' + str(args.outputfile) + '.pdf')

if __name__ == "__main__":
  args = arguments()
  setup_logging()
  parse_stockdata()
  setup_data_structures()
  if args.bollinger_bands:
    find_bollinger_band_events()
  else:
    find_price_events()
  output_events()
  if args.pdf:
    output_pdf()



# running it:
# python ./homework6.py --actual-close-price 6 --start 01012008 --end 31122009 --output ./eventstudy6.csv
# python ./homework6.py --actual-close-price 6 --start 01012008 --end 31122009 --output ./eventstudy6.csv --pdf True
# python -i ./homework6.py --actual-close-price 5 --start 01012008 --end 31122009 --sell-after 5 --output ./eventstudy5.csv

# running it (bollinger bands):
# python -i ./eventanalyzer.py --start 01012008 --end 31122009 --bollinger-bands -2.0 -2.0 1.1 --output ./eventstudy-bollinger-bands.csv
# python ./eventanalyzer.py --start 01012008 --end 31122009 --bollinger-bands -2.0 -2.0 1.0 --output ./eventstudy-bollinger-bands.csv --pdf true













